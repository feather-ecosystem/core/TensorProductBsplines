# TensorProductBsplines

[![pipeline status](https://gitlab.com/feather-ecosystem/TensorProductBsplines/badges/master/pipeline.svg)](https://gitlab.com/feather-ecosystem/TensorProductBsplines/-/commits/master)
[![coverage report](https://gitlab.com/feather-ecosystem/TensorProductBsplines/badges/master/coverage.svg)](https://gitlab.com/feather-ecosystem/TensorProductBsplines/-/commits/master)
[![codecov](https://codecov.io/gl/feather-ecosystem:core/TensorProductBsplines/branch/master/graph/badge.svg?token=2ySKiD55Xj)](https://codecov.io/gl/feather-ecosystem:core/TensorProductBsplines)
[![documentation (placeholder)](https://img.shields.io/badge/docs-latest-blue.svg)](https://feather-ecosystem.gitlab.io/core/TensorProductBsplines/)
