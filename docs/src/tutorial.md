# Tutorial
In the following we will define a mapping `F(x)`, where `F` is represented by a tensor-product B-spline on `[0...L]x[0...L]`. This mapping will interpolate a function `f(x) = 1 + sin(2π/L*x) * sin(2π/L*y)` at the Greville abscissa. We will showcase the following functionalities:

* projection on tensor-product spline spaces
* refinement of tensor-product spline spaces
* **todo!** convenience functions for plotting
* **todo!** computation of partial derivatives
* **todo!** tensor-product quadrature rules

!!! note
    This manual focuses on `TensorProductBsplines` package. Please have a look at the tutorial on `UnivariateSplines` first. There we introduce maps with `dimension == codimension == 1`.

## Tensor-product spline spaces
As mentioned above `TensorProductBsplines` are based upon the our `UnivariateSplines` package, which implements univariate spline spaces. Tensor-product spline spaces are constructed from univariate spline spaces in each parametric direction

```jldoctest tutorial1
julia> using UnivariateSplines, TensorProductBsplines

julia> p = Degree(2)
2

julia> univariate_spline_space = SplineSpace(p,4)
SplineSpace(degree = 2, interval = [0.0, 4.0], dimension = 6)

julia> @show univariate_spline_space.U;
univariate_spline_space.U = [0.0, 0.0, 0.0, 1.0, 2.0, 3.0, 4.0, 4.0, 4.0]

julia> L = univariate_spline_space.U[end]
4.0
```

From that we can construct the tensor-product space of the univariate spaces

```jldoctest tutorial1
julia> using CartesianProducts

julia> TensorProduct(univariate_spline_space, univariate_spline_space)
TensorProduct{2,SplineSpace{Float64}}((SplineSpace(degree = 2, interval = [0.0, 4.0], dimension = 6), SplineSpace(degree = 2, interval = [0.0, 4.0], dimension = 6)))

julia> S = univariate_spline_space ⨷ univariate_spline_space
TensorProduct{2,SplineSpace{Float64}}((SplineSpace(degree = 2, interval = [0.0, 4.0], dimension = 6), SplineSpace(degree = 2, interval = [0.0, 4.0], dimension = 6)))

```

## Tensor-product B-spline mappings
Having defined a function space we can now introduce a B-spline mapping `F`

```jldoctest tutorial1
julia> F = TensorProductBspline(S)
TensorProductBspline{Float64} defined on splinespaces
SplineSpace(degree = 2, interval = [0.0, 4.0], dimension = 6)
SplineSpace(degree = 2, interval = [0.0, 4.0], dimension = 6)


julia> F = refine(F, hRefinement(4))
TensorProductBspline{Float64} defined on splinespaces
SplineSpace(degree = 2, interval = [0.0, 4.0], dimension = 22)
SplineSpace(degree = 2, interval = [0.0, 4.0], dimension = 22)


julia> dimension(F)
2

julia> codimension(F)
1
```

This mapping will be used to interpolate the scalar function `f(x,y)`

```jldoctest tutorial1
julia> f = (x,y) -> 1 + sin(2π/L*x) * sin(2π/L*y)
#1 (generic function with 1 method)

julia> project!(f, F, Interpolation());

```