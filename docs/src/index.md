# TensorProductBsplines.jl
[![pipeline status](https://gitlab.com/feather-ecosystem/TensorProductBsplines/badges/master/pipeline.svg)](https://gitlab.com/feather-ecosystem/TensorProductBsplines/-/commits/master)
[![coverage report](https://gitlab.com/feather-ecosystem/TensorProductBsplines/badges/master/coverage.svg)](https://gitlab.com/feather-ecosystem/TensorProductBsplines/-/commits/master)
[![codecov](https://codecov.io/gl/feather-ecosystem:core/TensorProductBsplines/branch/master/graph/badge.svg?token=2ySKiD55Xj)](https://codecov.io/gl/feather-ecosystem:core/TensorProductBsplines)
[![documentation (placeholder)](https://img.shields.io/badge/docs-latest-blue.svg)](https://feather-ecosystem.gitlab.io/core/TensorProductBsplines/)

***

This package implements tensor-product B-splines maps on top of `SplineSpace` defined in [UnivariateSplines](https://gitlab.com/feather-ecosystem/core/UnivariateSplines/). The implementation includes:
* partial derivatives
* spline mappings of `dimension > 1` 
* plotting utilities
* projection of spaces of `codimension > 1` 
* tensor-product quadrature rules on top of rules in `UnivariateSplines`
* h-, p- and k-refinement
* boundary iterators 


!!! tip
    This package is a part of the [`Feather`](https://gitlab.com/feather-ecosystem) project. It is tightly integrated into the ecosystem of packages provided by Feather. If you are interested in applications of the functionality implemented in this package, please visit the main documentation of the [`ecosystem`](https://feather-ecosystem.gitlab.io/feather/)


For new users, we recommend to read the tutorial first and then review the API to learn about further implementational details like specialized iterators, low-level operators and data structures. Users interested in mathematical details of the implementation are kindly referred to the literature.

**Splines technology**
1. De Boor, Carl, et al. *A practical guide to splines*. Vol. 27. New York: springer-verlag, 1978.
2. Piegl, Les, and Wayne Tiller. *The NURBS book*. Springer Science & Business Media, 2012.
3. Schumaker, Larry. *Spline functions: basic theory*. Cambridge University Press, 2007.
4. Rogers, David F. *An introduction to NURBS: with historical perspective*. Elsevier, 2000.

**Weighted quadrature**
5. Sangalli, G., and Tani, M. *Matrix-free weighted quadrature for a computationally efficient isogeometric k-method*. Computer Methods in Applied Mechanics and Engineering 338 (Aug. 2018), 117–133.
6. Calabro, F., Sangalli, G., and Tani, M. *Fast formation of isogeometric Galerkin matrices by weighted quadrature*. Computer Methods in Applied Mechanics and Engineering 316 (Apr. 2017), 606–622.
7. Hiemstra, R. R., Sangalli, G., Tani, M., Calabro, F., and Hughes, T. J. *Fast formation and assembly of finite element matrices with application to isogeometric linear elasticity*. Computer Methods in Applied Mechanics and Engineering 355 (Oct. 2019), 234–260.
