using TensorProductBsplines, FeaInterfaces

package_info = Dict(
    "modules" => [TensorProductBsplines],
    "authors" => "Rene Hiemstra and contributors",
    "name" => "TensorProductBsplines.jl",
    "repo" => "https://gitlab.com/feather-ecosystem/core/TensorProductBsplines",
    "pages" => [
        "About"  =>  "index.md"
        "Tutorial"  =>  "tutorial.md"
        "API"  =>  "api.md"
    ],
)

DocMeta.setdocmeta!(TensorProductBsplines, :DocTestSetup, :(using TensorProductBsplines, UnivariateSplines); recursive=true)

# if docs are built for deployment, fix the doctests
# which fail due to round off errors...
if haskey(ENV, "DOC_TEST_DEPLOY") && ENV["DOC_TEST_DEPLOY"] == "yes"
    doctest(TensorProductBsplines, fix=true)
end
